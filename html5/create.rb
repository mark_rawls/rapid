#!/usr/bin/env ruby

def setupProject(projectName, config)
	system("git", "clone", "https://github.com/h5bp/html5-boilerplate.git")
	Dir.chdir("html5-boilerplate")

	files = Dir.glob("*")

	files.each do |fileName|
		mv(fileName, "..")
	end

	Dir.chdir("..")

	rm_rf("doc")
	rm_rf("html5-boilerplate")
	licenseFiles = Dir.glob("*.md")
	licenseFiles.each do |license|
		rm(license)
	end

	Dir.chdir("css")
	cp("main.css", "main.scss")
	File.open("mixins.scss", "w") do |mixins|
		mixins << %q&
@mixin transparency($degree) {
	filter: alpha(opacity=#{degree});
	-khtml-opacity: ($degree/100);
	-moz-opacity: ($degree/100);
	opacity: ($degree/100);
}

@mixin textShadow($color) {
	text-shadow: 1px 1px 2px $color;
	filter: dropshadow(color=$color, offx=1, offy=1);
}

@mixin colorTransition($color) {
	-webkit-transition-property: color;
	-moz-transition-property: color;
	transition-property: color;
	transition-duration: 0.5s;
	color: $color;
}

@mixin verticallyCentered {
	min-height: 6.5em;
	display: table-cell;
	vertical-align: middle;
}

@mixin boxShadow($xOffset, $yOffset, $blur, $spread, $color) {
	-webkit-box-shadow: $xOffset $yOffset $blur $spread $color;
	-moz-box-shadow: $xOffset $yOffset $blur $spread $color;
	box-shadow: $xOffset $yOffset $blur $spread $color;
}

@mixin insetBoxShadow($xOffset, $yOffset, $blur, $spread, $color) {
	-webkit-box-shadow: inset $xOffset $yOffset $blur $spread $color;
	-moz-box-shadow: inset $xOffset $yOffset $blur $spread $color;
	box-shadow: inset $xOffset $yOffset $blur $spread $color;
}

@mixin roundedCorners($radius) {
	-webkit-border-radius: $radius;
	-moz-border-radius: $radius;
	border-radius: $radius;
}

html, body {
	margin: $noMargins;
	padding: $noMargins;
	height: 100%;
	width: 100%;
	font-family: Frutiger, "Frutiger Linotype", Univers, Calibri, "Gill Sans", "Gill Sans MT", "Myriad Pro", Myriad, "DejaVu Sans Condensed", "Liberation Sans", "Nimbus Sans L", Tahoma, Geneva, "Helvetica Neue", Helvetica, Arial, sans-serif;
}&
	end

	Dir.chdir("..")

	mv("index.html", "index.php")
	File.open("globalfunctions.php", "w") do |functionFile|
		functionFile << %q&
function newStyle($url) {
	echo '		<link rel="stylesheet" type="text/css" href="' . $url . '" />' . PHP_EOL;
}

function linkStyles() {
	newStyle("css/normalize.css");
	newStyle("css/main.css");
}&
	end

	puts HighLine.color("Congratulations on your new website! Happy coding!", :green)
end