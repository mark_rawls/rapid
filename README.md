# README #

Rapid stands for Rapid Acclimation of Projects Into Deployment. Rapid has a built in package manager suited to upgrading existing suites, adding new ones, and removing old ones. More package functionality (such as remote/custom repositories) will be added in a later version.

### Readme Overview ###

* Command Overview
* Todo

### Command Overview ###

Rapid is completely self documenting. Simply run `rapid --help` to view a list of available commands. Sample output:


```
#!bash

$ rapid --help
Designed to be a much more user friendly and easily configurable version of
Quickly, with user defined support.

Usage:
        rapid [command] [options]
where [commands] are:
create -
        creates a new project in project directory (default: ~/Projects)
        syntax: rapid create [project type] [project name]
edit -
        uses preferred code editor (default: Sublime Text) to open up main source file
        syntax: rapid edit
        intelligently figures out which project is active
delete -
        deletes active project, after confirmation
        syntax: rapid delete
list -
        lists available project types
        syntax: rapid list
package -
        packages all supplied folders into support pack
        syntax: rapid package <folders> [zip file name]
  --install, -i <s>:   Service Package to install
      --version, -v:   Print version and exit
         --help, -h:   Show this message

$ rapid list
Available project types:

ruby
html5
java
rails
```

### TODO ###
* Remote repositories
* Template creation documentation
* Nothing! Pat yourself on the back.