#!/usr/bin/env ruby

def doSetupRoutine()
	Kernel.load($configDir + "/default.rb")
	defaultConfig = Configuration.for 'rapid'

	File.open("default.rb", "w") do |configFile|
		configFile << %Q@
#!/usr/bin/env ruby

Configuration.for('app') {
	editor "#{defaultConfig.editor}"
	browser "#{defaultConfig.browser}"
	projectDir "#{defaultConfig.projectDir}"
	extension ".rb"
}@
	end
end