#!/usr/bin/env ruby

def setupProject(projectName, config)
	File.open(projectName.camelize(:lower) + ".rb", "w") do |mainFile|
		mainFile.write(%Q@#!/usr/bin/env ruby

puts "Run 'rapid edit' to edit this project, or open up default.rb to change configuration for this project. Happy coding!"@)
	end

	File.open("default.rb", "w") do |configFile|
		configFile.write(File.read($templateDir + "/ruby/default.rb"))
	end

	system("chmod", "+x", projectName.camelize(:lower) + ".rb")
	system("chmod", "+x", "default.rb")

	mkdir("lib")

	puts HighLine.color("Project " + projectName + " is now set up! Enjoy!", :green)
end