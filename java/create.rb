#!/usr/bin/env ruby

def setupProject(projectName, config)
	File.open(projectName.camelize() + ".java", "w") do |mainFile|
		mainFile.write(%Q@
public class #{projectName.camelize()} {
	public static void main(String[] args) {

	}
}@)
	end

	File.open("default.rb", "w") do |configFile|
		configFile.write(File.read($templateDir + "/java/default.rb"))
	end

	system("chmod", "+x", "default.rb")

	puts HighLine.color("Project " + projectName + " is now set up! Enjoy!", :green)
end